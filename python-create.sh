#!/bin/bash

set -e -o pipefail

# Create new Python project with poetry and everything in place

function usage {
    echo "Usage:

    $0 <project-name> [options]

-p | --python <version-spec>    -- specify python version selector (default: ^3.10)
-a | --author <author>          -- author for pyproject.toml
-n | --package-name <name>      -- package name (defaults to project name)
-l | --license	<license>       -- license (default: MIT)
-g | --line-length              -- set line length for black and pylint
--no-pylint                     -- without pylint
--no-mypy                       -- without mypy
--no-doc                        -- without pdoc3
--no-poet                       -- without poethepoet
--no-bandit                     -- without bandit
--no-pytest                     -- without pytest and pytest-cov
--no-black                      -- without black
--no-docker                     -- do not create default Dockerfile
--no-git                        -- do not start git repository
--small                         -- same as --no-doc --no-bandit --no-pytest
--help                          -- display this message"
}

[ "$1" == "" ] && {
    usage
    exit 1
}

PACKAGE_DIR="$1"

[ -d "$PACKAGE_DIR" ] && {
    echo "Directory ${PACKAGE_DIR} already exists"
    exit 1
}

# Defaults
MYPY=YES
PYLINT=YES
POET=YES
BLACK=YES
PYTEST=YES
BANDIT=YES
DOC=YES
GIT=YES
DOCKER=YES
LINE_LENGTH=120
PYTHON_VERSION="^3.10"
LICENSE="MIT"
AUTHOR="Tomas Votava <info@tomasvotava.eu>"
PACKAGE_NAME="$1"

POSITIONAL_ARGS=()

while [[ $# -gt 0 ]]; do
    case $1 in
    -p | --python)
        PYTHON_VERSION="$2"
        shift
        shift
        ;;
    -a | --author)
        AUTHOR="$2"
        shift
        shift
        ;;
    -n | --package-name)
        PACKAGE_NAME="$2"
        shift
        shift
        ;;
    -l | --license)
        LICENSE="$2"
        shift
        shift
        ;;
    -g | --line-length)
        LINE_LENGTH="$2"
        shift
        shift
        ;;
    --no-pylint)
        PYLINT=NO
        shift
        ;;
    --no-mypy)
        MYPY=NO
        shift
        ;;
    --no-doc)
        DOC=NO
        shift
        ;;
    --no-poet)
        POET=NO
        shift
        ;;
    --no-bandit)
        BANDIT=NO
        shift
        ;;
    --no-pytest)
        PYTEST=NO
        shift
        ;;
    --no-black)
        BLACK=NO
        shift
        ;;
    --no-docker)
        DOCKER=NO
        shift
        ;;
    --no-git)
        GIT=NO
        shift
        ;;
    --small)
        BANDIT=NO
        DOC=NO
        PYTEST=no
        shift
        ;;
    --help)
        usage
        exit 0
        shift
        ;;
    -* | --*)
        echo "Unknown option $1"
        exit 1
        ;;
    *)
        POSITIONAL_ARGS+=("$1")
        shift
        ;;
    esac
done

set -- "${POSITIONAL_ARGS[@]}" # restore positional parameters
ORIGINAL_DIR=$(pwd)

echo "Package name: $PACKAGE_NAME"

mkdir "$PACKAGE_DIR"
cd "$PACKAGE_DIR"
mkdir "$PACKAGE_NAME"

touch "$PACKAGE_NAME/__init__.py"

echo '"""Version from pyproject.toml
"""

import os
import tomli

with open(os.path.join(os.path.dirname(__file__), "../pyproject.toml"), "rb") as file:
    pyproject = tomli.load(file)

__version__ = pyproject["tool"]["poetry"]["version"]

__all__ = ["__version__"]

if __name__ == "__main__":
    print(__version__)

' >"$PACKAGE_NAME/version.py"

echo -e "# ${PACKAGE_NAME}" >./README.md

poetry init \
    --quiet \
    --name "$PACKAGE_NAME" \
    --author "$AUTHOR" \
    --license "$LICENSE" \
    --python "$PYTHON_VERSION" \
    --no-interaction

DEVDEPS=""

[ "$MYPY" == "YES" ] && DEVDEPS+=" mypy "
[ "$PYLINT" == "YES" ] && DEVDEPS+=" pylint "
[ "$POET" == "YES" ] && DEVDEPS+=" poethepoet "
[ "$BLACK" == "YES" ] && DEVDEPS+=" black "
[ "$PYTEST" == "YES" ] && DEVDEPS+=" pytest pytest-cov "
[ "$BANDIT" == "YES" ] && DEVDEPS+=" bandit "
[ "$DOC" == "YES" ] && DEVDEPS+=" pdoc3 "

poetry add --dev $DEVDEPS
poetry add tomli

POET_TASKS="\n[tool.poe.tasks]\n"
POET_LINT=""

[ "$MYPY" == "YES" ] && {
    echo -e "[mypy]\nignore_missing_imports = true\n" >mypy.ini
    POET_TASKS+="mypy = \"mypy --config-file mypy.ini ${PACKAGE_NAME}\"\n"
    POET_LINT+='"mypy", '
}

[ "$PYLINT" == "YES" ] && {
    ./.venv/bin/pylint --generate-rcfile >./.pylintrc
    sed -i "s/max-line-length=100/max-line-length=${LINE_LENGTH}/g" ./.pylintrc
    POET_TASKS+="pylint = \"pylint ${PACKAGE_NAME}\"\n"
    POET_LINT+='"pylint", '
}

[ "$BLACK" == "YES" ] && {
    echo -e "\n[tool.black]\nline-length = ${LINE_LENGTH}" >>./pyproject.toml
    POET_TASKS+="format = \"black ${PACKAGE_NAME}\"\n"
}

[ "$PYTEST" == "YES" ] && {
    mkdir "tests"
    touch "tests/__init__.py"
    echo -e "\n[tool.pytest.ini_options]\n" \
        "testpaths = [\"tests\"]\n" \
        "addopts = \"-v --cov=${PACKAGE_NAME} --cov-report xml:cov.xml\"\n" >>./pyproject.toml
    echo -e "\n[tool.pyright]\nignore = [\"**/tests\"]" >>./pyproject.toml
    POET_TASKS+="test = \"python -m pytest\"\n"
    POET_TASKS+="coverage = \"coverage report\"\n"
    echo -e '[report]\nexclude_lines =\n\tpragma: no cover\n\tif TYPE_CHECKING:\n' >.coveragerc

}

[ "$BANDIT" == "YES" ] && {
    POET_TASKS+="bandit = \"bandit -r ${PACKAGE_NAME}/\"\n"
}

[ "$DOC" == "YES" ] && {
    POET_TASKS+="docs = \"pdoc3 --html --output-dir ./.public ${PACKAGE_NAME}\"\n"
}

[ "$POET" == "YES" ] && {
    echo -e "${POET_TASKS}" >>./pyproject.toml
}

[ "$POET_LINT" != "" ] && {
    echo -e "lint = [${POET_LINT}]\n" >>./pyproject.toml
}

echo ".public/" >.gitignore
curl -sSL https://raw.githubusercontent.com/github/gitignore/main/Python.gitignore >>.gitignore

[ "$GIT" == "YES" ] && {
    git init
}

[ "$DOCKER" == "YES" ] && {
    cp .gitignore .dockerignore
    echo 'FROM registry.gitlab.com/tomasvotava/python:3.10-slim
ENV POETRY_VIRTUALENVS_CREATE=false
WORKDIR /code
ADD pyproject.toml .
ADD poetry.lock .
RUN poetry install --no-dev
ADD . .

ENTRYPOINT ["python"]
' >Dockerfile
}
